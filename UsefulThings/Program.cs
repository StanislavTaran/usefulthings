﻿using UsefulThings.UnsafeAccess;
using UsefulThings.XmlSerialzation;

namespace UsefulThings
{
    class Program
    {
        static void Main(string[] args)
        {
            //XmlSerialization.TestXmlSerializer();
            UnsafeArrayAccess.GetUnsafeArrayAccessExample();
        }
    }
}
