﻿using System;

namespace UsefulThings.UnsafeAccess
{
    public static class UnsafeArrayAccess
    {
        private const int ELEM_NUM = 5;

        public static void GetUnsafeArrayAccessExample()
        {
            int[,] a2Dim = new int[ELEM_NUM, ELEM_NUM];

            Unsafe2DimArrayAccess(a2Dim);
        }

        private static unsafe int Unsafe2DimArrayAccess(int[,] a)
        {
            int sum = 0;
            fixed (int* pi = a)
            {
                for (int x = 0; x < ELEM_NUM; x++)
                {
                    int baseOfDim = x * ELEM_NUM;
                    for (int y = 0; y < ELEM_NUM; y++)
                    {
                        sum += pi[baseOfDim + y];
                    }
                }
            }

            return sum;
        }
    }
}
