﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace UsefulThings.XmlSerialzation
{
    public static class XmlSerialization
    {
        private class Order
        {
            public string Name { get; set; }
            public int OrderNumber { get; set; }
        }

        private static List<Order> GetOrders()
        {
            var orders = new List<Order>();
            var names = new string[] { "Jojo", "Rakhat", "Lokum", "Kurkuma", "Jakob" };

            for (int i = 0; i < 5; i++)
            {
                orders.Add(new Order()
                {
                    Name = names[i],
                    OrderNumber = i
                });
            }

            return orders;
        }

        public static void TestXmlSerializer()
        {
            List<Order> orders = GetOrders();
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            var xmlSerializer = new XmlSerializer(typeof(List<Order>));
            var file = File.Create($"{path}//orders.xml");

            xmlSerializer.Serialize(file, orders);

            file.Close();
        }
    }
}
