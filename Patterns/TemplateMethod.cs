﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Text;

namespace Patterns
{
    #region via DELEGATE
    public interface ILogSaver
    {
        void UploadLogEntries(IEnumerable<LogEntry> logEntries);
        void UploadExceptions(IEnumerable<ExceptionLogEntry> exceptions);
    }

    class LogSaverProxy : ILogSaver
    {
        class LogSaverClient : ClientBase<ILogSaver>
        {
            public ILogSaver LogSaver
            {
                get { return Channel; }
            }
        }

        public void UploadLogEntries(IEnumerable<LogEntry> logEntries)
        {
            UseProxyClient(c => c.UploadLogEntries(logEntries));
        }

        public void UploadExceptions(
        IEnumerable<ExceptionLogEntry> exceptions)
        {
            UseProxyClient(c => c.UploadExceptions(exceptions));
        }
        private void UseProxyClient(Action<ILogSaver> accessor)
        {
            var client = new LogSaverClient();
            try
            {
                accessor(client.LogSaver);
                client.Close();
            }
            catch (CommunicationException e)
            {
                client.Abort();
                throw new OperationFailedException(e);
            }
        }
    }
    #endregion via DELEGATE

    #region via Extensions
    public abstract class LogEntryBase
    {
        public DateTime EntryDateTime { get; internal set; }
        public Severity Severity { get; internal set; }
        public string Message { get; internal set; }
        // ExceptionLogEntry будет возвращать информацию об исключении
        public string AdditionalInformation { get; internal set; }
    }
    public static class LogEntryBaseEx
    {
        public static string GetText(this LogEntryBase logEntry)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("[{0}] ", logEntry.EntryDateTime)
                .AppendFormat("[{0}] ", logEntry.Severity)
                .AppendLine(logEntry.Message)
                .AppendLine(logEntry.AdditionalInformation);

            return sb.ToString();
        }
    }
    #endregion via Extensions

    #region new abstraction level
    public abstract class LogFileReaderBase : LogImporter, IDisposable
    {
        private readonly Lazy<Stream> _stream;
        protected LogFileReaderBase(string fileName)
        {
            _stream = new Lazy<Stream>(
            () => new FileStream(fileName, FileMode.Open));
        }
        public void Dispose()
        {
            if (_stream.IsValueCreated)
            {
                _stream.Value.Close();
            }
        }
        protected override sealed IEnumerable<string> ReadEntries(ref int position)
        {
            Contract.Assert(_stream.Value.CanSeek);
            if (_stream.Value.Position != position)
                _stream.Value.Seek(position, SeekOrigin.Begin);
            return ReadLineByLine(_stream.Value, ref position);
        }
        protected override abstract LogEntry ParseLogEntry(string stringEntry);
        private IEnumerable<string> ReadLineByLine(Stream stream, ref int position)
        {
            // Построчное чтение из потока ввода/вывода
        }
    }
    #endregion
}
